# DWEEBS

a website for [dweb-camp](https://dwebcamp.org/) 2022 peers to introduce themselves and things they care about

## Reverse Conference @ dweb

![](https://cdna.artstation.com/p/assets/images/images/030/146/976/large/shahzeb-khan-raza-shahzeb-khan-raza-gf.jpg)

My contribution to dweb camp is going to be a reverse-conference - instead of being like a normal conference where you get talked at for 90% of the time, and only 10% connecting… over muffins… in a hallway… instead of that, you get people to post videos of their talks before the conference, so people can watch ahead of time, and then spend 10% of the time at the conference getting talked at (being coordinated), and 90% connecting!

I’m gonna make a shitty website and host em (here it is!)

## Contributing

Contributions are welcome from Dweb 2022 attendees.
Pick a section to add a video to:

1. [Tell us about your project](./tell-us-about-your-project.md) (5mins)
2. [Soap Box](./soap-box.md) - rant about an issue you care about
3. [Longer talks](./longer-talks.md) - maybe you have a conference talk or something you'd like to share

Open a [pull-request](https://gitlab.com/mixmix/dweebs), fork the repo, or email dweebs@protozoa.nz with a youtube link.

### Styling

If you want to improve the styling/ formatting, that's also welcome.
My #1 priority is accessibility - for people visiting, and people editing.


---

Artist : https://www.artstation.com/shahzebkhanraza
